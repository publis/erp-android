package com.jackfruit.tbsdemo;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class LaunchActivity extends AppCompatActivity {

    private final int splash_display_length = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lauch);
        LaunchActivity self = this;
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent = new Intent(LaunchActivity.this, WebActivity.class);
                LaunchActivity.this.startActivity(mainIntent);
                LaunchActivity.this.finish();

//                Intent mainIntent = new Intent(LaunchActivity.this, PreloadService.class);
//                startService(mainIntent);
            }
        },splash_display_length);
    }
}